import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../services/api-service.service';
import {StorageServiceService}from '../services/storage-service.service';
import {ToastService}from '../services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formData: any;
  resp: any;
  loader = false;

  constructor(private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) {
    if(!this.formData){
      this.formData = {
        userName:'',
        password:''
      }
    }
  }


  ngOnInit(): void {
  }

  login(){
    console.log(this.formData.userName)
    if(this.formData.userName ==='' || this.formData.password ===''){
        this.toastService.showToast('Fill all fields');
    }else{
      this.loader = true;
      this.apiService.login(JSON.stringify(this.formData)).subscribe(res => {
        this.resp = res;
        this.loader = false;
        if(this.resp.responseInfo.responseCode === '000'){
          if(this.resp.userInfo.status === 'active') {
            this.router.navigate(['/app/new-sale']);
            this.storageService.saveInfo('userData', JSON.stringify(this.resp));
          }
          else if(this.resp.userInfo.status === 'new'){
            this.router.navigate(['/reset']);
          }else{
            this.toastService.showToast('Contact administrator');
          }

        }else{
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
  }

}
