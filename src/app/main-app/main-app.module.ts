import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainAppRoutingModule } from './main-app-routing.module';
import { MainAppComponent } from './main-app/main-app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NavbarComponent } from './navbar/navbar.component';
import { NewSaleComponent } from './new-sale/new-sale.component';
import { SaleTodayComponent } from './sale-today/sale-today.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { SalesHistoryComponent } from './sales-history/sales-history.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { UsersComponent } from './users/users.component';
import { ShopComponent } from './shop/shop.component';
import { ProductsComponent } from './products/products.component';
import {MatButtonModule} from '@angular/material/button';
import { ProductsDialogComponent } from './dialogs/products-dialog/products-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatRadioModule} from '@angular/material/radio';
import { ShopsDialogComponent } from './dialogs/shops-dialog/shops-dialog.component';
import { UserDialogComponent } from './dialogs/user-dialog/user-dialog.component';
import { SubmittedComponent } from './submitted/submitted.component';


@NgModule({
  declarations: [MainAppComponent, NavbarComponent, NewSaleComponent, SaleTodayComponent, SalesHistoryComponent, UsersComponent, ShopComponent, ProductsComponent, ProductsDialogComponent, ShopsDialogComponent, UserDialogComponent, SubmittedComponent],
  entryComponents: [ ProductsDialogComponent, ShopsDialogComponent, UserDialogComponent ],
  imports: [
    CommonModule,
    MainAppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    TypeaheadModule.forRoot(),
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatRadioModule
  ]
})
export class MainAppModule { }
