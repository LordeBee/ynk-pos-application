import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewSaleComponent } from './new-sale/new-sale.component';
import { SaleTodayComponent } from './sale-today/sale-today.component';
import { SalesHistoryComponent } from './sales-history/sales-history.component';
import { UsersComponent } from './users/users.component';
import { ShopComponent } from './shop/shop.component';
import { ProductsComponent } from './products/products.component';
import { SubmittedComponent } from './submitted/submitted.component';

const routes: Routes = [
  {path:'new-sale', component: NewSaleComponent},
  {path:'sale-today', component: SaleTodayComponent},
  {path:'sales-history', component: SalesHistoryComponent},
  {path:'users', component: UsersComponent},
  {path:'shop', component: ShopComponent},
  {path:'products', component: ProductsComponent},
  {path:'submitted', component: SubmittedComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAppRoutingModule { }
