import {Component, OnInit, Inject, ɵConsole, ViewChild, Output, EventEmitter, Input} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {StorageServiceService} from '../../../services/storage-service.service';
import {ToastService} from '../../../services/toast.service';
import {ApiServiceService} from '../../../services/api-service.service';


export interface ApproveDialogData {
  id: string;



}

@Component({
  selector: 'app-products-dialog',
  templateUrl: './products-dialog.component.html',
  styleUrls: ['./products-dialog.component.scss']
})
export class ProductsDialogComponent implements OnInit {

  selectedOption = '1';
  formData: any;

  loadin = false;

  @Output() onApprove = new EventEmitter();
  userInfo: any;
  addResp: any;

  constructor(public dialogRef: MatDialogRef<ProductsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: ApproveDialogData, private storageServiceService: StorageServiceService, private toastService: ToastService, private apiService: ApiServiceService) {

    if(!this.formData){
      this.formData = {
        id:'',
        quantity:'',
        price:'',
        createdBy:'',
        reason: '',
        itemType: 'product',
        itemId: ''

      }
    }
  }

  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageServiceService.getInfo('userData'));
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  add(): void{

    if(this.formData.price === '' || this.formData.quantity === ''){
      this.toastService.showToast('Fill all fields');
    }
    else{
      this.loadin = true;
      this.formData.id = this.data.id;
      this.formData.createdBy =  this.userInfo.userInfo.userName;
      this.apiService.updateProducts(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        if(this.addResp.responseInfo.responseCode === '000'){
          this.onApprove.emit('000');
          this.toastService.showToast('Updated successfully');
          this.onNoClick();
        }else{
          this.loadin = false;
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.loadin = false;
        this.toastService.showToast('Please try again');
      });
    }

  }

  update(): void{

    if(this.formData.price === '' ){
      this.toastService.showToast('Fill all fields');
    }
    else{
      this.loadin = true;
      this.formData.id = this.data.id;
      this.formData.createdBy =  this.userInfo.userInfo.userName;

      this.apiService.updatePrice(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        if(this.addResp.responseInfo.responseCode === '000'){
          this.onApprove.emit('000');
          this.toastService.showToast('Update successfully');
          this.onNoClick();
        }else{
          this.loadin = false;
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.loadin = false;
        this.toastService.showToast('Please try again');
      });
    }

  }

  delete(): void{

    if(this.formData.reason === '' ){
      this.toastService.showToast('Enter a reason');
    }
    else{
      this.loadin = true;
      this.formData.itemId = this.data.id;
      this.formData.createdBy =  this.userInfo.userInfo.userName;

      this.apiService.deleteRecord(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        if(this.addResp.responseInfo.responseCode === '000'){
            this.onApprove.emit('000');
            this.toastService.showToast('Deleted successfully');
            this.onNoClick();
        }else{
          this.loadin = false;
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.loadin = false;
        this.toastService.showToast('Please try again');
      });
    }
  }

}
