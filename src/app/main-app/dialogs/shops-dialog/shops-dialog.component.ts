import {Component, OnInit, Inject, ɵConsole, ViewChild, Output, EventEmitter, Input} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {StorageServiceService} from '../../../services/storage-service.service';
import {ToastService} from '../../../services/toast.service';
import {ApiServiceService} from '../../../services/api-service.service';


export interface ApproveDialogData {
  id: string;



}

@Component({
  selector: 'app-shops-dialog',
  templateUrl: './shops-dialog.component.html',
  styleUrls: ['./shops-dialog.component.scss']
})
export class ShopsDialogComponent implements OnInit {


  selectedOption = '1';
  formData: any;

  loadin = false;

  @Output() onApprove = new EventEmitter();
  userInfo: any;
  addResp: any;


  constructor(public dialogRef: MatDialogRef<ShopsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: ApproveDialogData, private storageServiceService: StorageServiceService, private toastService: ToastService, private apiService: ApiServiceService) {

    if(!this.formData){
      this.formData = {
        createdBy: '',
        reason: '',
        itemType: 'shop',
        itemId: ''

      };
    }
  }


  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageServiceService.getInfo('userData'));
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  delete(): void{

    if(this.formData.reason === '' ){
      this.toastService.showToast('Enter a reason');
    }
    else{
      this.loadin = true;
      this.formData.itemId = this.data.id;
      this.formData.createdBy =  this.userInfo.userInfo.userName;

      this.apiService.deleteRecord(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        if(this.addResp.responseInfo.responseCode === '000'){
          this.onApprove.emit('000');
          this.toastService.showToast('Deleted successfully');
          this.onNoClick();
        }else{
          this.loadin = false;
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.loadin = false;
        this.toastService.showToast('Please try again');
      });
    }
  }
}
