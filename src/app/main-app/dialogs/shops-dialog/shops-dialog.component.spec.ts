import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsDialogComponent } from './shops-dialog.component';

describe('ShopsDialogComponent', () => {
  let component: ShopsDialogComponent;
  let fixture: ComponentFixture<ShopsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
