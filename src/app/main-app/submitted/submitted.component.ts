import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';



export interface userInfo {
  id: string;
  bankName: any;
  amount: string;
  addedBy: string;
  dateCreated: any;

}

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss']
})
export class SubmittedComponent implements OnInit {

  displayedColumns: string[] = ['id', 'bankName', 'amount', 'addedBy', 'dateCreated'];
  dataSource: MatTableDataSource<userInfo>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userInfo: any;
  resp: any;
  userData: Array<any>;
  isData = false;
  formData: any;
  response: any;
  shops: Array<any>;
  loader = false;
  addResp: any;

  constructor( private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router, private dialog: MatDialog) {

    this.userData = [];

  }

  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));


    this.apiService.getBankSubmitted(this.userInfo.userInfo.shopId).subscribe(res => {
      this.resp = res;
      if(this.resp.responseInfo.responseCode === '000'){
        this.isData = true;
        this.userData = this.resp.shopInfo;
        this.dataSource = new MatTableDataSource<userInfo>(this.userData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
