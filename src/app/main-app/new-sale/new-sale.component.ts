import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {ToastService} from '../../services/toast.service';
import {Router} from '@angular/router';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { PrintService, UsbDriver, WebPrintDriver } from 'ng-thermal-print';
import { PrintDriver } from 'ng-thermal-print/lib/drivers/PrintDriver';
import {DatePipe} from '@angular/common';

const {Impresora} = require('../../../assets/Impresora');
declare var $: any;


@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',
  providers: [DatePipe],
  styleUrls: ['./new-sale.component.scss']
})
export class NewSaleComponent implements OnInit {

  userInfo: any;
  products: Array<any>;
  resp: any;
  selectedValue: string;


  items = 0;
  amount = 0;
  amountPaid = 0;
  change = 0;
  formData: any;
  loader = false;

  response: any;

  status: boolean = false;
  usbPrintDriver: UsbDriver;
  connected = false;
  availablePrint = false;


  constructor( private datePipe: DatePipe, private printService: PrintService, private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) {
    this.products = [];
    this.usbPrintDriver = new UsbDriver();
    if(!this.formData){
      this.formData = {
        saleId:'',
        createdBy:'',
        shopId:'',
        shopName:'',
        paymentType:'CASH',
        amountPaid:'',
        saleItems:[]
      }
    }
   }

   saleItems  = [];



  ngOnInit(): void {

    $( "#quantity" ).focus(function() {
      alert( "Handler for .focus() called." );
    });

    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));
    console.log(this.userInfo);

    this.apiService.getShopProducts(this.userInfo.userInfo.shopId).subscribe(res => {
      this.resp = res;
      console.log(this.resp)
      if(this.resp.responseInfo.responseCode === '000'){
        this.products = this.resp.productInfo;
      }
    })
  }



  ngAfterViewChecked() {

    const elem = '#quantity' + (this.saleItems.length - 1);


    $(elem).keypress(function(e) {
      if(e.which == 13) {
        $( "#selectProd" ).focus();
      }
    });



  }

  onSelect(event: TypeaheadMatch): void {

    const data = event.item;
    this.saleItems.push({ productId: data.id, productName: data.name, quantity:1, unitPrice: data.price, totalPrice: parseFloat(data.price) });
    this.selectedValue = '';
    this.displayTotal();
    this.calculateChange();



  }

  remove(index: any){
    this.saleItems.splice(index, 1);
    this.displayTotal();
    this.calculateChange();


  }

  addQuantity(index: any){
    this.saleItems[index].totalPrice = this.saleItems[index].quantity * this.saleItems[index].unitPrice;
    this.displayTotal();
    this.calculateChange();

  }


  displayTotal(){
    this.items = this.saleItems.reduce(function(prev, cur) {
      return prev + cur.quantity;
    }, 0);

    this.amount = this.saleItems.reduce(function(prev, cur) {
      return prev + cur.totalPrice;
    }, 0);
  }

  calculateChange(){
    this.change = this.amountPaid - this.amount;
  }

  clearTill(){
    this.saleItems = [];
  }



  submit(){

    var d = new Date();
    var n = d.getTime();
    this.formData.amountPaid = this.amountPaid;
    this.formData.saleId =  this.userInfo.userInfo.userName + n;
    this.formData.createdBy = this.userInfo.userInfo.userName;
    this.formData.shopId = this.userInfo.userInfo.shopId;
    this.formData.shopName = this.userInfo.userInfo.shopName;
    this.formData.saleItems = this.saleItems;

    if(this.change >= 0){
      this.loader = true;
        this.apiService.addSale(JSON.stringify(this.formData)).subscribe(res => {
        this.response = res;
        this.loader = false;
        if(this.response.responseInfo.responseCode === '000'){
          this.toastService.showToast('Sale recorded successfully');
          if(this.availablePrint){
            this.printOurs();
            this.saleItems = [];
            this.amountPaid = 0;
            this.amount = 0;
            this.change = 0;
          }else{
            this.saleItems = [];
            this.amountPaid = 0;
            this.amount = 0;
            this.change = 0;
          }



        }else{
          this.toastService.showToast('Please try again');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
    else{
      this.toastService.showToast('Please enter amount paid');
    }


  }

  requestUsb() {
    this.usbPrintDriver.requestUsb().subscribe(result => {
        this.printService.setDriver(this.usbPrintDriver, 'ESC/POS');
        this.connected = true;
    });
  }


  getValues(array: Array<any>){
    const count = array.length;
    for(let i = 0; i < count; i++){
      return array[i].productName;
    }
  }

  print(type: any): void {
    // let grossTotal = (this.amount -(this.amount * 0.175));
    // let nhl = this.amount * 0.025;
    // let VAT = this.amount * 0.125;
    //
    // const print = this.printService.init()
    // .setBold(true)
    // .setJustification('center')
    // .writeLine(type)
    // .writeLine(this.userInfo.userInfo.shopName)
    // .writeLine('Koforidua')
    // .writeLine('0241110741')
    // .writeLine('')
    // .writeLine('')
    // .setJustification('left')
    // .writeLine('Receiept')
    // .writeLine('Receiept #: '+  this.response.responseInfo.invoiceNumber)
    // .writeLine('Date/Time:' + this.datePipe.transform(new Date(), 'MMM d, y, h:mm:ss a'))
    // .writeLine('Sale Type: '+  this.formData.paymentType)
    // .writeLine('Cashier: '+  this.userInfo.userInfo.name)
    // .writeLine('')
    // .writeLine('')
    // .writeLine('------------------------------------------------')
    // .setJustification('center')
    // .writeLine('Order Description')
    // .writeLine('------------------------------------------------')
    // .setJustification('left')
    //
    // .writeLine('QTY     NAME       ')
    // this.saleItems.forEach(item => {
    //   print.setJustification('left')
    //   print.writeLine(item.quantity+ '     '  +   item.productName )
    //   print.setJustification('right')
    //   print.writeLine('GHS '  + item.totalPrice.toFixed(2))
    //
    // })
    // print.setJustification('left')
    // print.writeLine('')
    // print.writeLine('------------------------------------------------')
    // print.writeLine('Gross Total: GHS '+  grossTotal.toFixed(2))
    // print.writeLine('NHIL (2.5%): GHS '+  nhl.toFixed(2))
    // print.writeLine('Get Fund (2.5%): GHS '+  nhl.toFixed(2))
    // print.writeLine('VAT (12.5%): GHS '+  VAT.toFixed(2))
    // print.writeLine('')
    // print.writeLine('------------------------------------------------')
    // print.writeLine('Total: GHS '+  this.amount.toFixed(2))
    // print.writeLine('Paid: GHS '+  this.formData.amountPaid.toFixed(2))
    // print.writeLine('Change: GHS '+  this.change.toFixed(2))
    // print.writeLine('------------------------------------------------')
    // print.writeLine('')
    // .setJustification('center')
    // print.writeLine('Software by: Jireh Developers - 0241110741')
    // print.feed(4)
    //     .cut('full')
    //     .flush();
  }


  printOurs(): void{
    let grossTotal = (this.amount -(this.amount * 0.175));
    let nhl = this.amount * 0.025;
    let VAT = this.amount * 0.125;

    const RUTA_API = 'http://localhost:8000';
    const impresora = new Impresora(RUTA_API);
    impresora.cut();
    impresora.setAlign('center');
    impresora.write(this.userInfo.userInfo.shopName + '\n');
    impresora.write('Koforidua' + '\n');
    impresora.write('0241110741' + '\n');
    impresora.write('' + '\n');
    impresora.write(''  + '\n');
    impresora.setAlign('left');
    impresora.write('Receiept' + '\n');
    impresora.write('Receiept #: '+  this.response.responseInfo.invoiceNumber + '\n');
    impresora.write('Date/Time:' + this.datePipe.transform(new Date(), 'MMM d, y, h:mm:ss a')  + '\n');
    impresora.write('Sale Type: '+  this.formData.paymentType + '\n');
    impresora.write('Cashier: '+  this.userInfo.userInfo.name + '\n');
    impresora.write('' + '\n');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------'  + '\n');
    impresora.setAlign('center');
    impresora.write('Order Description' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.setAlign('left');

    impresora.write('QTY     NAME       ' + '\n');
    this.saleItems.forEach(item => {
      impresora.setAlign('left');
      impresora.write(item.quantity+ '     '  +   item.productName  + '\n');
      impresora.setAlign('right');
      impresora.write('GHS '  + item.totalPrice.toFixed(2) + '\n');

    });
    impresora.setAlign('left');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('Gross Total: GHS '+  grossTotal.toFixed(2) + '\n');
    impresora.write('NHIL (2.5%);: GHS '+  nhl.toFixed(2) + '\n');
    impresora.write('Get Fund (2.5%);: GHS '+  nhl.toFixed(2) + '\n');
    impresora.write('VAT (12.5%);: GHS '+  VAT.toFixed(2) + '\n');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('Total: GHS '+  this.amount.toFixed(2) + '\n');
    impresora.write('Paid: GHS '+  this.formData.amountPaid.toFixed(2) + '\n');
    impresora.write('Change: GHS '+  this.change.toFixed(2) + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('' + '\n');
    impresora.cut();
    impresora.cutPartial();

    impresora.setAlign('center');
    impresora.write('OUR COPY' + '\n');
    impresora.write(this.userInfo.userInfo.shopName + '\n');
    impresora.write('Koforidua' + '\n');
    impresora.write('0241110741' + '\n');
    impresora.write('' + '\n');
    impresora.write(''  + '\n');
    impresora.setAlign('left');
    impresora.write('Receiept' + '\n');
    impresora.write('Receiept #: '+  this.response.responseInfo.invoiceNumber + '\n');
    impresora.write('Date/Time:' + this.datePipe.transform(new Date(), 'MMM d, y, h:mm:ss a')  + '\n');
    impresora.write('Sale Type: '+  this.formData.paymentType + '\n');
    impresora.write('Cashier: '+  this.userInfo.userInfo.name + '\n');
    impresora.write('' + '\n');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------'  + '\n');
    impresora.setAlign('center');
    impresora.write('Order Description' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.setAlign('left');

    impresora.write('QTY     NAME       ' + '\n');
    this.saleItems.forEach(item => {
      impresora.setAlign('left');
      impresora.write(item.quantity+ '     '  +   item.productName  + '\n');
      impresora.setAlign('right');
      impresora.write('GHS '  + item.totalPrice.toFixed(2) + '\n');

    });
    impresora.setAlign('left');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('Gross Total: GHS '+  grossTotal.toFixed(2) + '\n');
    impresora.write('NHIL (2.5%);: GHS '+  nhl.toFixed(2) + '\n');
    impresora.write('Get Fund (2.5%);: GHS '+  nhl.toFixed(2) + '\n');
    impresora.write('VAT (12.5%);: GHS '+  VAT.toFixed(2) + '\n');
    impresora.write('' + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('Total: GHS '+  this.amount.toFixed(2) + '\n');
    impresora.write('Paid: GHS '+  this.formData.amountPaid.toFixed(2) + '\n');
    impresora.write('Change: GHS '+  this.change.toFixed(2) + '\n');
    impresora.write('------------------------------------------------' + '\n');
    impresora.write('' + '\n');


    impresora.feed(2);
    impresora.cut();
    impresora.cutPartial(); // use both because sometimes cut and/or cutPartial do not work
    impresora.end()
      .then(valor => {
        console.log("Response: " + valor);
      });
  }

  addPrinter(): void{
    this.availablePrint = true;
  }

}
