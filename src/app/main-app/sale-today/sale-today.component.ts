import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService}from '../../services/storage-service.service';
import {ToastService}from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';


export interface saleData {
  saleId: string;
  id: string;
  productId: any;
  productName: string;
  quantity: string;
  unitPrice: string;
  totalPrice: string;
  createdBy: string;
  shopId: any;
  shopName: any;
  dateCreated: any;
}

@Component({
  selector: 'app-sale-today',
  templateUrl: './sale-today.component.html',
  styleUrls: ['./sale-today.component.scss']
})
export class SaleTodayComponent implements OnInit {

  displayedColumns: string[] = ['id', 'productName', 'quantity', 'unitPrice', 'totalPrice','dateCreated'];
  dataSource: MatTableDataSource<saleData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userInfo: any;
  resp: any;
  userSales: Array<any>;
  isData = false;
  cashInHand  = 0;
  formData: any;
  loader = false;
  addResp: any;

  constructor( private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) {
    this.userSales = [];
    if(!this.formData){
      this.formData = {
        bankName: 'PRUDENTIAL DRINKS SUPERMARKET LTD',
        amount: '',
        shopId: '',
        addedBy: ''

      }
    }
  }

  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));
    console.log(this.userInfo);

    this.apiService.getTodaySales(this.userInfo.userInfo.shopId).subscribe(res => {
      this.resp = res;
      this.isData = true;
      if(this.resp.responseInfo.responseCode === '000'){
       this.userSales = this.resp.saleInfo.filter(sale => sale['createdBy'] === this.userInfo.userInfo.userName);
       this.dataSource = new MatTableDataSource<saleData>(this.userSales);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
       this.cashInHand = this.userSales.reduce((accum, item) => accum + parseFloat(item.totalPrice), 0);
      }
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  submit(): void{

    this.formData.amount = this.resp.saleInfo.reduce((accum, item) => accum + parseFloat(item.totalPrice), 0);
    this.formData.shopId = this.userInfo.userInfo.shopId;
    this.formData.addedBy = this.userInfo.userInfo.userName;
    this.loader = true;
    this.apiService.submitToBank(JSON.stringify(this.formData)).subscribe(res => {
      this.addResp = res;
      this.loader = true;
      if(this.addResp.responseInfo.responseCode === '000'){
        this.router.navigate(['/app/submitted']);
      }else{
        this.toastService.showToast('Please try again');
      }

    },error=>{
      this.toastService.showToast('Please try again');
    });

  }

}
