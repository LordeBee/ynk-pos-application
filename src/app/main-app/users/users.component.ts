import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService}from '../../services/storage-service.service';
import {ToastService}from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {UserDialogComponent} from '../dialogs/user-dialog/user-dialog.component';
import {MatDialog} from '@angular/material/dialog';


export interface userInfo {
  id: string;
  name: any;
  username: string;
  phone: string;
  shopId: any;
  shopName: any;
  status: any;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {


  displayedColumns: string[] = ['id', 'name', 'username', 'phone', 'shopName', 'status' , 'edit'];
  dataSource: MatTableDataSource<userInfo>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userInfo: any;
  resp: any;
  userData: Array<any>;
  isData = false;
  formData: any;
  response: any;
  shops: Array<any>;
  loader = false;
  addResp: any;

  constructor( private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router, private dialog: MatDialog) {

    this.userData = [];

    if(!this.formData){
      this.formData = {
        firstName:'',
        lastName:'',
        userName:'',
        phone:'',
        shopId:'',
        shopName:'',
        role:''
      }
    }

    this.shops = [];

   }

  ngOnInit(): void {

    this.apiService.getUsers().subscribe(res => {
      this.resp = res;
      this.isData = true;
      if(this.resp.responseInfo.responseCode === '000'){
       this.userData = this.resp.userInfo;
       this.dataSource = new MatTableDataSource<userInfo>(this.userData);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
      }
    })

    this.apiService.getShops().subscribe(res => {
      this.response = res;
      if(this.response.responseInfo.responseCode === '000'){
        this.shops = this.response.shops;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  getShopName(event: any){
    const selected = this.shops.filter(shop => shop['id'] === this.formData.shopId);
    this.formData.shopName = selected[0].name;
  }


  add(){
    if(this.formData.firstName === '' || this.formData.lastName === '' || this.formData.userName === '' ||
    this.formData.phone === '' || this.formData.shopId === '' || this.formData.role === '' ){
      this.toastService.showToast('Fill all fields');
    }else{
      this.loader = true;
      this.apiService.createUser(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        this.loader = false;
        if(this.addResp.responseInfo.responseCode === '000'){
          this.formData.firstName = '';
          this.formData.lastName = '';
          this.formData.userName = '';
          this.formData.phone = '';
          this.formData.shopId = '';
          this.formData.role = '' ;

          this.isData = false;
          this.toastService.showToast('User added successfully');
          this.apiService.getUsers().subscribe(res => {
            this.resp = res;
            this.isData = true;
            if(this.resp.responseInfo.responseCode === '000'){
             this.userData = this.resp.userInfo;
             this.dataSource = new MatTableDataSource<userInfo>(this.userData);
             this.dataSource.paginator = this.paginator;
             this.dataSource.sort = this.sort;
            }
          })
        }else{
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }

  }

  edit(id: any): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '600px',
      height: '500px',
      data: {
        id: id
      }
    });

    dialogRef.componentInstance.onApprove.subscribe(res => {
      if (res === '000') {
        this.isData = false;
        this.apiService.getUsers().subscribe(res => {
          this.resp = res;
          this.isData = true;
          if(this.resp.responseInfo.responseCode === '000'){
            this.userData = this.resp.userInfo;
            this.dataSource = new MatTableDataSource<userInfo>(this.userData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        });

      }
    });

  }

  }
