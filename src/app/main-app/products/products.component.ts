import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService}from '../../services/storage-service.service';
import {ToastService}from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import {ProductsDialogComponent} from '../dialogs/products-dialog/products-dialog.component';


export interface userInfo {
  id: string;
  name: any;
  type: string;
  price: string;
  shopId: any;
  shopName: any;
  quantity: any;
  createdBy: string;
  dateCreated: any;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'type', 'price', 'shopName','quantity', 'dateCreated', 'edit'];
  dataSource: MatTableDataSource<userInfo>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userInfo: any;
  resp: any;
  userData: Array<any>;
  isData = false;
  formData: any;
  response: any;
  shops: Array<any>;
  loader = false;
  addResp: any;

  constructor(private apiService: ApiServiceService,
    private storageService: StorageServiceService,
              private toastService: ToastService,
              private router: Router,
              private dialog: MatDialog) {

    this.userData = [];

    if(!this.formData){
      this.formData = {
        name:'',
        type:'',
        price:'',
        createdBy:'',
        shopId:'',
        shopName:'',
        quantity:''
      }
    }

    this.shops = [];
  }

  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));


    this.apiService.getProducts().subscribe(res => {
      this.resp = res;
      this.isData = true;
      if(this.resp.responseInfo.responseCode === '000'){
       this.userData = this.resp.productInfo;
       this.dataSource = new MatTableDataSource<userInfo>(this.userData);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
      }
    })

    this.apiService.getShops().subscribe(res => {
      this.response = res;
      if(this.response.responseInfo.responseCode === '000'){
        this.shops = this.response.shops;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  getShopName(event: any){
    const selected = this.shops.filter(shop => shop['id'] === this.formData.shopId);
    this.formData.shopName = selected[0].name;
  }

  add(){

    if(this.formData.name === '' || this.formData.type === '' || this.formData.price === '' ||
    this.formData.shopId === '' || this.formData.quantity === ''){
      this.toastService.showToast('Fill all fields');
    }else{
      this.formData.createdBy = this.userInfo.userInfo.userName;
      this.loader = true;
      this.apiService.addProduct(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        this.loader = false;
        if(this.addResp.responseInfo.responseCode === '000'){
          this.formData.name = '';
          this.formData.type = '';
          this.formData.price = '';
          this.formData.shopId = '';
          this.formData.quantity = '' ;

          this.isData = false;
          this.toastService.showToast('Product added successfully');
          this.apiService.getProducts().subscribe(res => {
            this.resp = res;
            this.isData = true;
            if(this.resp.responseInfo.responseCode === '000'){
             this.userData = this.resp.productInfo;
             this.dataSource = new MatTableDataSource<userInfo>(this.userData);
             this.dataSource.paginator = this.paginator;
             this.dataSource.sort = this.sort;
            }
          })
        }else{
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
  }

  edit(id: any): void{
    const dialogRef = this.dialog.open(ProductsDialogComponent,{
      width: '600px',
      height: '500px',
      data: {
        id: id
      }
    });
    dialogRef.componentInstance.onApprove.subscribe(res => {
      if (res === '000'){
        this.isData = false;
        this.apiService.getProducts().subscribe(res => {
          this.resp = res;
          this.isData = true;
          if(this.resp.responseInfo.responseCode === '000'){
            this.userData = this.resp.productInfo;
            this.dataSource = new MatTableDataSource<userInfo>(this.userData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        })
      }
    })
  }

}
