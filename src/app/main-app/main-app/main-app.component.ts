import { Component, OnInit } from '@angular/core';
import {StorageServiceService} from '../../services/storage-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.scss']
})
export class MainAppComponent implements OnInit {


  userInfo: any;

  constructor(private storageService: StorageServiceService, private router: Router) { }

  ngOnInit(): void {
    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));
  }

  logout(){
    this.storageService.clearInfo('userData');
    this.router.navigate(['/login']);
  }

}
