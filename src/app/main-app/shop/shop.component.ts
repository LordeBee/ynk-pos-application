import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService}from '../../services/storage-service.service';
import {ToastService}from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ShopsDialogComponent} from '../dialogs/shops-dialog/shops-dialog.component';
import {MatDialog} from '@angular/material/dialog';


export interface userInfo {
  id: string;
  name: any;
  location: string;
  createdBy: string;
  dateCreated: any;

}


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'location', 'createdBy', 'dateCreated', 'edit'];
  dataSource: MatTableDataSource<userInfo>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userInfo: any;
  resp: any;
  userData: Array<any>;
  isData = false;

  formData: any;
  loader = false;
  addResp: any;

  constructor(private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router, private dialog: MatDialog) {

    if(!this.formData){
      this.formData = {
        name:'',
        location:'',
        createdBy:''
      }
    }

   }


  ngOnInit(): void {

    this.userInfo = JSON.parse(this.storageService.getInfo('userData'));


    this.apiService.getShops().subscribe(res => {
      this.resp = res;
      if(this.resp.responseInfo.responseCode === '000'){
        this.isData = true
        this.userData = this.resp.shops;
        this.dataSource = new MatTableDataSource<userInfo>(this.userData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addShop(){


    if(this.formData.name === '' || this.formData.location === ''){
      this.toastService.showToast('Fill all fields');
    }else{
      this.formData.createdBy = this.userInfo.userInfo.userName;
      this.loader = true;
      this.apiService.addShop(JSON.stringify(this.formData)).subscribe(res => {
        this.addResp = res;
        this.loader = false;
        if(this.addResp.responseInfo.responseCode === '000'){

          this.formData.name = '';
          this.formData.location = '';

          this.isData = false;
          this.toastService.showToast('Shop added successfully');
          this.apiService.getShops().subscribe(res => {
            this.resp = res;
            if(this.resp.responseInfo.responseCode === '000'){
              this.isData = true
              this.userData = this.resp.shops;
              this.dataSource = new MatTableDataSource<userInfo>(this.userData);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            }
          });
        }else{
          this.toastService.showToast('Invalid Credentials');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
  }

  edit(id: any): void{
    const dialogRef = this.dialog.open(ShopsDialogComponent, {
      width: '600px',
      height: '500px',
      data: {
        id: id
      }
    });

    dialogRef.componentInstance.onApprove.subscribe(res => {
      if (res === '000') {
        this.isData = false;
        this.apiService.getShops().subscribe(res => {
          this.resp = res;
          if(this.resp.responseInfo.responseCode === '000'){
            this.isData = true
            this.userData = this.resp.shops;
            this.dataSource = new MatTableDataSource<userInfo>(this.userData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        });

      }
    });
  }

}
