import { Component, OnInit, ViewChild } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService}from '../../services/storage-service.service';
import {ToastService}from '../../services/toast.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {DatePipe} from '@angular/common';

import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../services/ format-datepicker';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


export interface saleData {
  saleId: string;
  id: string;
  productId: any;
  productName: string;
  quantity: string;
  unitPrice: string;
  totalPrice: string;
  createdBy: string;
  shopId: any;
  shopName: any;
  dateCreated: any;
}

@Component({
  selector: 'app-sales-history',
  templateUrl: './sales-history.component.html',
  providers: [DatePipe, {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}],
  styleUrls: ['./sales-history.component.scss']
})
export class SalesHistoryComponent implements OnInit {

  startDate: any;
  endDate: any;
  shops: Array<any>;
  resp: any;

  shopID = new FormControl();

  userSales: Array<any>;
  isData = false;
  loading = false;

  response: any;


  displayedColumns: string[] = ['id', 'productName', 'quantity', 'unitPrice', 'totalPrice', 'shopName', 'dateCreated'];
  dataSource: MatTableDataSource<saleData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;



  constructor(private datePipe: DatePipe,private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) { 

    this.endDate = this.datePipe.transform(new Date(), 'y-MM-dd');
    this.startDate = this.datePipe.transform(new Date(+ new Date - 12096e5), 'y-MM-dd');
    this.shops = [];
    this.shopID.setValue('1');
 



  }

  ngOnInit(): void {

    this.loading = true;
    this.apiService.getShops().subscribe(res => {
      this.resp = res;
      if(this.resp.responseInfo.responseCode === '000'){
        this.shops = this.resp.shops;
        this.apiService.getHistorySales(this.datePipe.transform(this.startDate, 'y-MM-dd'), this.datePipe.transform(this.endDate, 'y-MM-dd')).subscribe(res => {
          this.response = res;
          this.isData = true;
          this.loading = false;
          if(this.response.responseInfo.responseCode === '000'){
           this.userSales = this.response.saleInfo.filter(sale => sale['shopId'] === this.shopID.value);
           this.dataSource = new MatTableDataSource<saleData>(this.userSales);
           this.dataSource.paginator = this.paginator;
           this.dataSource.sort = this.sort;
          }
        })
      }
    });
  }


  search(){
    if(this.endDate === '' || this.startDate === ''){

    }else{
      this.loading = true;
      this.apiService.getHistorySales(this.datePipe.transform(this.startDate, 'y-MM-dd'), this.datePipe.transform(this.endDate, 'y-MM-dd')).subscribe(res => {
        this.response = res;
        this.isData = true;
        this.loading = false;
        if(this.response.responseInfo.responseCode === '000'){
         this.userSales = this.response.saleInfo.filter(sale => sale['shopId'] === this.shopID.value);
         this.dataSource = new MatTableDataSource<saleData>(this.userSales);
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
        }
      })

    }
    
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
