import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../services/api-service.service';
import {StorageServiceService}from '../services/storage-service.service';
import {ToastService}from '../services/toast.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  formData: any;
  resp: any;
  loader = false;
  reTypePassword: any;

  constructor(private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) {
    if(!this.formData){
      this.formData = {
        code:'',
        password:'',
        userName:''
      }
    }

    this.reTypePassword = '';
  }


  ngOnInit(): void {

  this.formData.userName =  this.storageService.getInfo('userName');
  }

  login(){

    if(this.formData.password ==='' || this.formData.code ==='' || this.reTypePassword === '' ){
        this.toastService.showToast('Fill all fields');
    }else if(this.formData.password  !== this.reTypePassword){
      this.toastService.showToast('Passwords do not much');
    }
    else{
      this.loader = true;
      this.apiService.changePassword(JSON.stringify(this.formData)).subscribe(res => {
        this.resp = res;
        this.loader = false;
        if(this.resp.responseInfo.responseCode === '000'){
          this.toastService.showToast('Success login with new password');
          this.router.navigate(['/login']);
        }else{
          this.toastService.showToast('Invalid Username');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
  }

}
