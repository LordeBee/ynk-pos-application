import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  // private baseUrl = 'https://ynkltd.com/';
  private baseUrl = 'http://192.168.1.2:4200/pos/';

  // private baseUrl = '/api/';
  private loginUrl = `${this.baseUrl}login.php`;
  private resetUrl = `${this.baseUrl}sendOtp.php`;
  private changeUrl = `${this.baseUrl}reset.php`;
  private shopProductsUrl = `${this.baseUrl}getProductByShopId.php?id=`;
  private addSaleUrl = `${this.baseUrl}addSale.php`;
  private todaySaleUrl = `${this.baseUrl}todaySale.php?id=`;
  private getShopsUrl = `${this.baseUrl}getShops.php`;
  private historyUrl = `${this.baseUrl}salesByDate.php?`;
  private getUsersUrl = `${this.baseUrl}getUsers.php`;
  private createUserUrl = `${this.baseUrl}createUser.php`;
  private addShopUrl = `${this.baseUrl}addShop.php`;
  private addProductUrl = `${this.baseUrl}addProduct.php`;
  private getProductsUrl = `${this.baseUrl}getProducts.php`;
  private deleteRecordUrl = `${this.baseUrl}deleteRecord.php`;
  private updatePriceUrl = `${this.baseUrl}updatePrice.php`;
  private updateProductsUrl = `${this.baseUrl}updateProducts.php`;
  private submitToBankUrl = `${this.baseUrl}submitToBank.php`;
  private shopSubmitUrl = `${this.baseUrl}getBankSubmitted.php?id=`;


  private httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Access-Control-Allow-Origin', '*')
  .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
  .set(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  .set('Cache-Control', 'no-cache');


private options = {
  headers: this.httpHeaders
};


constructor(private _http:HttpClient) { }


  login(data: any){
    return this._http.post(this.loginUrl, data, this.options);
  }


  resetPassword(data: any){
    return this._http.post(this.resetUrl, data, this.options);
  }


  changePassword(data: any){
    return this._http.post(this.changeUrl, data, this.options);
  }

  getShopProducts(id: any){
    return this._http.get(this.shopProductsUrl + id, this.options);
  }

  addSale(data: any){
    return this._http.post(this.addSaleUrl, data, this.options);
  }

  getTodaySales(id: any){
    return this._http.get(this.todaySaleUrl + id, this.options);
  }

  getShops(){
    return this._http.get(this.getShopsUrl, this.options);
  }

  getHistorySales(startDate: any, endDate: any){
    return this._http.get(this.historyUrl + `startDate=${startDate}&endDate=${endDate}`, this.options);
  }

  getUsers(){
    return this._http.get(this.getUsersUrl, this.options);
  }

  createUser(data: any){
    return this._http.post(this.createUserUrl, data, this.options);
  }

  addShop(data: any){
    return this._http.post(this.addShopUrl, data, this.options);
  }

  addProduct(data: any){
    return this._http.post(this.addProductUrl, data, this.options);
  }

  getProducts(){
    return this._http.get(this.getProductsUrl, this.options);
  }

  deleteRecord(data: any){
    return this._http.post(this.deleteRecordUrl, data, this.options);
  }

  updatePrice(data: any){
    return this._http.post(this.updatePriceUrl, data, this.options);
  }

  updateProducts(data: any){
    return this._http.post(this.updateProductsUrl, data, this.options);
  }


  submitToBank(data: any){
    return this._http.post(this.submitToBankUrl, data, this.options);
  }

  getBankSubmitted(id: any){
    return this._http.get(this.shopSubmitUrl + id, this.options);
  }
}
