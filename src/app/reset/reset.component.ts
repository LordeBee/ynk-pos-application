import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../services/api-service.service';
import {StorageServiceService}from '../services/storage-service.service';
import {ToastService}from '../services/toast.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

 
  formData: any;
  resp: any;
  loader = false;

  constructor(private apiService: ApiServiceService, private storageService: StorageServiceService, private toastService: ToastService, private router: Router) { 
    if(!this.formData){
      this.formData = {
        userName:''
      }
    }
  }


  ngOnInit(): void {

  }

  login(){
    console.log(this.formData.userName)
    if(this.formData.userName ===''){
        this.toastService.showToast('Fill all fields');
    }else{
      this.storageService.saveInfo('userName', this.formData.userName);
      this.loader = true;
      this.apiService.resetPassword(JSON.stringify(this.formData)).subscribe(res => {
        this.resp = res;
        this.loader = false;
        if(this.resp.responseInfo.responseCode === '000'){
          this.router.navigate(['/change']);
        }else{
          this.toastService.showToast('Invalid Username');
        }
      },error=>{
        this.toastService.showToast('Please try again');
      });
    }
  }

}
