import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ResetComponent } from './reset/reset.component';
import { MainAppComponent } from './main-app/main-app/main-app.component'


const routes: Routes = [
  {path:'login', component: LoginComponent},
  {path:'change', component: ChangePasswordComponent},
  {path:'reset', component: ResetComponent},
  {
    path: 'app',
    component: MainAppComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./main-app/main-app.module').then(m => m.MainAppModule)
      }
    ]
  },

  { path: '**', redirectTo: '/login', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
